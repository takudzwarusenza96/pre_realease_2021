home_to_start_station_code = ['C1', 'C2', 'C3', 'C4', 'C5']
home_to_start_station_price = [1.5, 3.0, 4.5, 6.0, 8.0]
start_station_to_end_station_code = ['M1', 'M2', 'M3', 'M4', 'M5']
start_station_to_end_station_price = [5.75, 12.5, 22.25, 34.5, 45]
end_station_to_destination_code =['F1', 'F2', 'F3', 'F4', 'F5']
end_station_to_destination_price =[1.5, 3, 4.5, 6, 8]

passenger_acc_number=[]
passenger_names=[]

booking_passenger_account_number=[]
booking_start_time=[]
booking_hss_code=[]
booking_ses_code=[]
booking_ed_code=[]
booking_id_number=[]

############################################################################################################################

def create_passenger(name):
    passenger_names.append(name)
    
    #auto generating passenger account  number
    acc_number='P'+str( len(passenger_names))
    passenger_acc_number.append(acc_number)
    
############################################################################################################################
        
def booking(passenger_account_number,start_time,hss_code,ses_code,ed_code):
    #test the validity of the input
    if passenger_account_number in passenger_acc_number:
        if passenger_account_number not in booking_passenger_account_number:
            if start_time<=23 and start_time>=0:
                if hss_code in home_to_start_station_code:
                    if ses_code in start_station_to_end_station_code:
                        if ed_code in end_station_to_destination_code:
                
                           #now allow for the booking
                           booking_passenger_account_number.append(passenger_account_number)
                           booking_start_time.append(start_time)
                           booking_hss_code.append(hss_code)
                           booking_ses_code.append(ses_code)
                           booking_ed_code.append(ed_code)
                           
                           #associted price
                           hss_price = home_to_start_station_price[home_to_start_station_code.index(hss_code)]
                           ses_price = start_station_to_end_station_price[start_station_to_end_station_code.index(ses_code)]
                           ed_price  = end_station_to_destination_price[end_station_to_destination_code.index(ed_code)]
                    
                           #auto generating booking id number
                           booking_id='B'+str(len(booking_passenger_account_number))
                           booking_id_number.append(booking_id)
                           print('The auto generated number:',booking_id)
                           
                           # Total Price
                           total_price=hss_price+ses_price+ed_price
                           if start_time>10:
                               total_price= total_price*0.6
                           print('You have successfully booked \n Your total price is,',total_price)
                              
                        else:
                            print('Station Code does not exist')
                    else:
                        print('Station Code does not exist')
                else:
                    print('Station Code does not exist')
            else:
                print("Time not in range")
        else:
            print('you have already booked')
    else:
        print('You are not a passenger')
###########################################################################################################################      
#Tests        
create_passenger('Director')
booking('P1',11,'C5','M5','F5')







